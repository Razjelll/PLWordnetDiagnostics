from database.database import Database
from diagnostics.diagnostics import Diagnostics
from embeddings.embeddings_set import EmbeddingsSet
from embeddings.limit_embeddings import Limiter
from embeddings.saver import EmbeddingsSetSaver
from wordnet.relation_set import RelationSet
from wordnet.saver import RelationSetSaver, SynsetSaver
from wordnet.utils import WordnetSynset
import os
import logging

database_configuration_path = 'database/configuration.cfg'
output_folder = 'output'
synsets_path = 'output/wordnet/synsets.json'
relation_set_path = 'output/wordnet/relation_set.txt'
relations_configuration_path = 'wordnet/relations_set_configuration.cfg'

input_embeddings_path = '/media/roman/SeagateExpansion/Pobrane/Do pracy/kgr_mwe/kgr10-model-sg-300-mwe-lemmatized.vec'
limited_embeddings_path = 'output/embeddings/embeddings.vec'
embeddings_set_path = 'output/embeddings/embeddings_set.txt'

output_path = 'output/intersection.txt'
def main():
    # __create_output_folders(output_folder)
    # __run_wordnet_set()
    # __run_embeddings_set()
    __start_diagnostics()


def __run_wordnet_set():
    logging.info('Createing synsets')
    synsets = __get_synsets()
    logging.info('Searching relations')
    related_words_map = __create_related_words_map(synsets)
    logging.info('Saving relations')
    RelationSetSaver.save(related_words_map, synsets, relation_set_path)
    logging.info('Finished')

def __run_embeddings_set():
    synsets = __get_synsets()
    units = __get_units(synsets)
    words = set([unit.word for unit in units])
    result = EmbeddingsSet.generate(limited_embeddings_path, words, 20)
    EmbeddingsSetSaver.save(result, embeddings_set_path)

def __start_diagnostics():
    Diagnostics.run(relation_set_path, embeddings_set_path, synsets_path, output_path)

def __get_units(synsets):
    units = []
    for synset_id, synset in synsets.items():
        units.extend(synset.units)
    return units


def limit_embeddings(units):
    words = set([unit.word for unit in units])
    Limiter.limit(input_embeddings_path, limited_embeddings_path, words)

def __get_synsets():
    if not os.path.isfile(synsets_path):
        synsets = __load_synsets_from_database()
        SynsetSaver.save(synsets, synsets_path)
    else:
        synsets = SynsetSaver.load(synsets_path)
    return synsets

def __load_synsets_from_database():
    database = Database(database_configuration_path)
    units = database.find_units([1, 2, 3, 4])
    units_relations = database.find_units_relations()
    synsets_relation = database.find_synsets_relations()
    synsets = WordnetSynset.create_synsets(units, units_relations, synsets_relation)
    return synsets

def __create_related_words_map(synsets):
    related_words_map = {}
    for id, synset in synsets.items():
        relations_set = RelationSet.create_set(synset, synsets, relations_configuration_path)
        related_words_map[id] = relations_set
    return related_words_map

def __create_output_folders(output_path):
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    wordnet_folder = output_path + '/wordnet'
    embeddings_folder = output_path + '/embeddings'
    if not os.path.exists(wordnet_folder):
        os.makedirs(wordnet_folder)
    if not os.path.exists(embeddings_folder):
        os.makedirs(embeddings_folder)

main()