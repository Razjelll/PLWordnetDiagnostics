from database.database_helper import DBHelper
from wordnet.models import Unit, Relations


class Database:

    def __init__(self, configuration_path):
        self.__db_helper = DBHelper(configuration_path)

    def find_units(self, parts_of_speech):
        statement = 'SELECT ID, lemma, variant, pos, domain, SYN_ID ' \
                    'FROM lexicalunit LU JOIN unitandsynset US ON LU.ID = US.LEX_ID ' \
                    'WHERE pos IN :poses'

        result_set = self.__db_helper.get_session().execute(statement, {'poses': parts_of_speech})
        units = self.__get_units_from_result_set(result_set)
        result_set.close()
        return units

    def __get_units_from_result_set(self, result_set):
        units = []
        for row in result_set:
            id, word, variant, pos, domain, synset = row
            unit = Unit(id, word,variant, pos, domain, synset)
            units.append(unit)
        return units

    def find_units_relations(self, relations=None):
        result_set = self.__get_units_relations_result_set(relations)
        relations = self.__get_relations_from_result_set(result_set)
        result_set.close()
        return relations

    def __get_units_relations_result_set(self, relations=None):
        parameters_map = {}
        statement = 'SELECT PARENT_ID, CHILD_ID, REL_ID ' \
                    'FROM lexicalrelation'
        if relations:
            statement += ' WHERE REL_ID IN:relations'
            parameters_map['relations'] = relations
        return self.__db_helper.get_session().execute(statement, relations)

    def __get_relations_from_result_set(self, result_set):
        relations = Relations()
        for row in result_set:
            target, source, relation = row
            relations.add(source, target, relation)
        return relations

    def find_synsets_relations(self, relations=None):
        result_set = self.__get_synsets_relations_result_set(relations)
        relations = self.__get_relations_from_result_set(result_set)
        result_set.close()
        return relations

    def __get_synsets_relations_result_set(self, relations):
        statement = 'SELECT PARENT_ID, CHILD_ID, REL_ID ' \
                    'FROM synsetrelation'
        parameters_map = {}
        if relations:
            statement += ' WHERE REL_ID IN :relations'
            parameters_map['relations'] = relations

        return self.__db_helper.get_session().execute(statement, parameters_map)
