import pymysql
import sqlalchemy
from sqlalchemy import orm
from configparser import ConfigParser


class DBHelper:

    def __init__(self, configuration_path):
        pymysql.install_as_MySQLdb()
        configuration_map = ConfigReader.read(configuration_path)
        engine = self.__create_engine(configuration_map)
        self.__session = self.__create_session(engine)

    def __create_engine(self, configuration_map):
        host = configuration_map[ConfigReader.HOST]
        port = configuration_map[ConfigReader.PORT]
        database = configuration_map[ConfigReader.DATABASE]
        username = configuration_map[ConfigReader.USERNAME]
        password = configuration_map[ConfigReader.PASSWORD]

        settings = {'drivername': 'mysql', 'host': host, 'port': port, 'database': database,
                    'username': username, 'password': password, 'query': {'charset': 'utf8mb4'}}

        url = sqlalchemy.engine.url.URL(**settings)
        engine = sqlalchemy.create_engine(url, echo=False)
        return engine

    def __create_session(self, engine):
        session = sqlalchemy.orm.sessionmaker(bind=engine)
        return session()

    def get_session(self):
        return self.__session

    def close(self):
        self.__session.close()

class ConfigReader:

    HOST = 'host'
    PORT = 'port'
    DATABASE = 'database'
    USERNAME = 'username'
    PASSWORD = 'password'

    @staticmethod
    def read(path):
        SECTION = 'database'
        parser = ConfigParser()
        parser.read(path)

        host = parser.get(SECTION, ConfigReader.HOST)
        port = int(parser.get(SECTION, ConfigReader.PORT))
        database = parser.get(SECTION, ConfigReader.DATABASE)
        username = parser.get(SECTION, ConfigReader.USERNAME)
        password = parser.get(SECTION, ConfigReader.PASSWORD)

        result_map = {
            ConfigReader.HOST: host,
            ConfigReader.PORT: port,
            ConfigReader.DATABASE: database,
            ConfigReader.USERNAME: username,
            ConfigReader.PASSWORD: password
        }

        return result_map
