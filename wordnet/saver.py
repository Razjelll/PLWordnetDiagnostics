import json
import pickle

from wordnet.models import Unit, Synset


class WordnetSaver:

    @staticmethod
    def save_synsets_map(synsets_map, path):
        pickle.dump(synsets_map, open(path, 'wb'))

    @staticmethod
    def load_synsets_map(path):
        return pickle.load(open(path, 'rb'))


class SynsetSaver:
    DOMAIN = 'domain'
    SYNSET = 'synset'
    POS = 'pos'
    WORD = 'word'
    VARIANT = 'variant'
    RELATIONS = 'relations'
    UNITS = 'units'
    ID = 'id'

    @staticmethod
    def save(synsets, path):
        result = {}
        for id, synset in synsets.items():
            units = SynsetSaver.__create_synset_units(synset)
            relations = SynsetSaver.__create_synset_relations(synset)
            synset_dict = SynsetSaver.__create_synset_dict(id, relations, units)
            result[id] = synset_dict

        SynsetSaver.__save_file(path, result)

    @staticmethod
    def __save_file(path, result):
        with open(path, 'w') as file:
            json_str = json.dumps(result)
            file.write(json_str)

    @staticmethod
    def __create_synset_relations(synset):
        relations = {}
        for type, related_synsets in synset.relations.items():
            related_ids = [synset.id for synset in related_synsets]
            relations[int(type)] = related_ids
        return relations

    @staticmethod
    def __create_synset_units(synset):
        units = []
        for unit in synset.units:
            units_dict = SynsetSaver.__get_unit_json(unit)
            units.append(units_dict)
        return units

    @staticmethod
    def __create_synset_dict(id, relations, units):
        synset_dict = {}
        synset_dict[SynsetSaver.ID] = id
        synset_dict[SynsetSaver.UNITS] = units
        synset_dict[SynsetSaver.RELATIONS] = relations
        return synset_dict

    @staticmethod
    def load(path):

        with open(path, 'r') as file:
            dictionary = json.load(file)
            result = SynsetSaver.__create_synsets(dictionary)
            SynsetSaver.__insert_synset_relations(dictionary, result)
        return result

    @staticmethod
    def __insert_synset_relations(dictionary, result):
        for id, synset_dict in dictionary.items():
            relations = {}
            for type, related_ids in synset_dict[SynsetSaver.RELATIONS].items():
                related_synsets = [result[int(id)] for id in related_ids]
                relations[int(type)] = related_synsets
            synset = result[int(id)]
            synset.relations = relations

    @staticmethod
    def __create_synsets(dictionary):
        result = {}
        for id, synset_dict in dictionary.items():
            synset = Synset(id)
            units = []
            for unit_dict in synset_dict[SynsetSaver.UNITS]:
                unit = SynsetSaver.__load_units_from_json(unit_dict)
                units.append(unit)
            synset.units = units
            result[int(id)] = synset
        return result

    @staticmethod
    def __load_units_from_json(unit_dict):
        id = int(unit_dict[SynsetSaver.ID])
        word = unit_dict[SynsetSaver.WORD]
        variant = int(unit_dict[SynsetSaver.VARIANT])
        pos = unit_dict[SynsetSaver.POS]
        synset = unit_dict[SynsetSaver.SYNSET]
        domain = unit_dict[SynsetSaver.DOMAIN] if SynsetSaver.DOMAIN in unit_dict else None

        unit = Unit(id, word, variant, pos, domain, synset)
        return unit

    @staticmethod
    def __get_unit_json(unit):
        unit_dict = {}
        unit_dict[SynsetSaver.ID] = unit.id
        unit_dict[SynsetSaver.POS] = unit.pos
        unit_dict[SynsetSaver.WORD] = unit.word
        unit_dict[SynsetSaver.VARIANT] = unit.variant
        unit_dict[SynsetSaver.SYNSET] = unit.synset
        if unit.domain:
            unit_dict[SynsetSaver.DOMAIN] = unit.domain
        return unit_dict


class RelationSetSaver:
    DELIMITER = '='
    RELATIONS_DELIMITER = ','
    IDENTIFIER_DELIMITER = ':'

    @staticmethod
    def save(relations_set_map, synsets, path):
        with open(path, 'w') as file:
            for id, related_words in relations_set_map.items():
                identifier = RelationSetSaver.__get_synset_identifier(id, synsets)
                file.write(identifier)
                file.write(RelationSetSaver.DELIMITER)
                words = RelationSetSaver.RELATIONS_DELIMITER.join(related_words)
                file.write(words)
                file.write('\n')

    @staticmethod
    def __get_synset_identifier(synset_id, synsets):
        if synset_id in synsets:
            synset = synsets[synset_id]
            word = synset.units[0].word
            variant = synset.units[0].variant
            string = '{0}{3}{1}{3}{2}'.format(str(synset_id), word, variant, RelationSetSaver.IDENTIFIER_DELIMITER)
            return string
        return ''

    @staticmethod
    def load(path):
        result = {}
        with open(path, 'r') as file:
            for line in file:
                line = line.rstrip()
                parts = line.split(RelationSetSaver.DELIMITER)
                id = RelationSetSaver.__get_synsets_ids(parts[0])
                words_str = parts[1]
                related_words = words_str.split(RelationSetSaver.RELATIONS_DELIMITER)
                result[id] = related_words
        return result

    @staticmethod
    def __get_synsets_ids(synset_identifier):
        parts = synset_identifier.split(RelationSetSaver.IDENTIFIER_DELIMITER)
        return int(parts[0])
