from wordnet.models import Synset, Relations


class WordnetWords:

    @staticmethod
    def get_list_of_words(units):
        return [unit.word for unit in units]

    @staticmethod
    def get_list_of_words_by_domain(units, domains):
        return [unit.word for unit in units if unit.domain in domains]


class WordnetSynset:

    @staticmethod
    def create_synsets(units, units_relations: Relations, synsets_relations: Relations):
        synsets, units_ids = WordnetSynset.__create_synsets_and_units_map(units)
        WordnetSynset.__insert_relations(synsets, synsets_relations, units_ids, units_relations)
        return synsets
        # return units.__create_synsets_list(synsets)

    @staticmethod
    def __create_synsets_list(self, synsets):
        return [synset for id, synset in synsets.items()]

    @staticmethod
    def __insert_relations(synsets, synsets_relations, units_ids, units_relations):
        for id, synset in synsets.items():
            synset_relations = synsets_relations.get_relations_by_id(id)
            relations = WordnetSynset.__get_synset_relations(synset_relations, synsets)
            WordnetSynset.__update_synset_relations(synset, relations)
            for unit in synset.units:
                unit_relations = units_relations.get_relations_by_id(unit.id)
                relations = WordnetSynset.__get_units_relations(unit_relations, synsets, units_ids)
                WordnetSynset.__update_synset_relations(synset, relations)

    @staticmethod
    def __create_synsets_and_units_map(units):
        synsets = {}
        units_ids = {}
        for unit in units:
            synset_id = unit.synset
            if synset_id not in synsets:
                synsets[synset_id] = Synset(synset_id)
            synsets[synset_id].units.append(unit)
            units_ids[unit.id] = unit
        return synsets, units_ids

    @staticmethod
    def __update_synset_relations(synset, relations):
        synset_relations = synset.relations
        for type, related_synsets in relations.items():
            if type in synset_relations:
                synset_relations[type].extend(related_synsets)
            else:
                synset_relations[type] = related_synsets

    @staticmethod
    def __get_synset_relations(relations, synsets):
        result_relations = {}
        for relation in relations:
            type_id = relation[Relations.TYPE]
            target_id = relation[Relations.TARGET]
            if target_id in synsets:
                target_synset = synsets[target_id]
                if type_id in result_relations:
                    result_relations[type_id].append(target_synset)
                else:
                    result_relations[type_id] = [target_synset]
        return result_relations

    @staticmethod
    def __get_units_relations(relations, synsets, units_ids):
        result_relations = {}
        for relation in relations:
            type_id = relation[Relations.TYPE]
            target_id = relation[Relations.TARGET]
            if target_id in units_ids:
                target_unit = units_ids[target_id]
                synset_id = target_unit.id
                if synset_id in result_relations:
                    synset = synsets[synset_id]
                    if type_id in result_relations:
                        result_relations[type_id].append(
                            synset)  # TODO: prawdopodobnie będzie trzeba dodać sprawdzanie, czy ten synset jest już zawarty
                    else:
                        result_relations[type_id] = [synset]
        return result_relations
