from configparser import ConfigParser

class Configuration:

    def __init__(self):
        self.hypernyms = []
        self.hypernyms_level = 1
        self.hyponyms = []
        self.hyponyms_level = 1
        self.synonyms = []
        self.relations = []

    def init(self, path):
        SECTION = 'relations'
        parser = ConfigParser()
        parser.read(path)

        self.hypernyms = self.__get_values(parser.get(SECTION, 'hypernyms'))
        self.hypernyms_level = int(parser.get(SECTION, 'hypernyms_level'))
        self.hyponyms = self.__get_values(parser.get(SECTION, 'hyponyms'))
        self.hyponyms_level = int(parser.get(SECTION, 'hyponyms_level'))
        self.synonyms = self.__get_values(parser.get(SECTION, 'synonyms'))
        self.relations = self.__get_values(parser.get(SECTION, 'relations'))

    def __get_values(self, text):
        values = text.split(',')
        return [int(value) for value in values]


class RelationSet:


    @staticmethod
    def create_set(synset, synsets_map, configuration_path):
        # TODO: zorientować się, czy relacje powinny być zriobione dla jednostek czy synsetów. Przemyśleć to dokładnie
        configuration = RelationSet.__load_configuration(configuration_path)

        synonims_words = RelationSet.__find_related_synonyms(synset, synsets_map, configuration)
        hypernyms_words = RelationSet.__find_related_hypernyms(synset, synsets_map, configuration)
        hyponyms_words = RelationSet.__find_related_hyponyms(synset, synsets_map, configuration)
        related_words = RelationSet.__find_other_related_words(synset, synsets_map, configuration)

        all_related_units = synonims_words + hypernyms_words + hyponyms_words + related_words
        return set(all_related_units)

    @staticmethod
    def __find_related_synonyms(synset, synsets_map, configuration):
        synonyms_units = RelationSet.__get_synonyms_from_synset(synset)
        synonyms_words = RelationSet.__get_words_from_units(synonyms_units)

        synonyms_relations_words = RelationSet.__get_others_relations(synset, synsets_map, configuration.synonyms)
        return synonyms_words + synonyms_relations_words

    @staticmethod
    def __find_related_hypernyms(synset, synsets_map, configuration):
        return RelationSet.__find_structure_relations_words(synset, synsets_map, configuration.hypernyms, configuration.hypernyms_level)

    @staticmethod
    def __find_related_hyponyms(synset, synsets_map, configuration):
        return RelationSet.__find_structure_relations_words(synset, synsets_map, configuration.hyponyms, configuration.hyponyms_level)

    @staticmethod
    def __find_structure_relations_words(synset, synsets_map, relations_types, level):
        synsets = RelationSet.__get_synsets_from_structure_relations(synset, synsets_map, relations_types, level)
        units = RelationSet.__get_units_from_synsets_list(synsets)
        words = RelationSet.__get_words_from_units(units)
        return words

    @staticmethod
    def __find_other_related_words(synset, synsets_map, relations):
        related_synsets = RelationSet.__get_others_relations(synset, synsets_map, relations)
        related_units = RelationSet.__get_units_from_synsets_list(related_synsets)
        related_words = RelationSet.__get_words_from_units(related_units)
        return related_words

    @staticmethod
    def __load_configuration(configuration_path):
        configuration = Configuration()
        configuration.init(configuration_path)
        return configuration

    @staticmethod
    def __get_units_from_synsets_list(synsets_list):
        units = []
        for synset in synsets_list:
            units.extend(synset.units)
        return units

    @staticmethod
    def __get_words_from_units(units):
        return [unit.word for unit in units]

    @staticmethod
    def __get_synonyms_from_synset(synset):
        return synset.units

    @staticmethod
    def __get_synsets_from_structure_relations(synset, synsets_map, relation_types, structure_level):
        stack = []
        level = structure_level
        stack.append((synset, level))
        used_units = set()
        result = []
        while len(stack) > 0:
            current_entry = stack.pop()
            current_synset = current_entry[0]
            current_level = current_entry[1]
            # TODO: refaktoryzacja
            for type in relation_types:
                if type in current_synset.relations:
                    for related_synset in current_synset.relations[type]:
                        if current_level > 0 and related_synset not in used_units:
                            stack.append((related_synset, current_level - 1))
                            result.append(related_synset)
                            used_units.add(related_synset)
        return result


    @staticmethod
    def __get_hypernyms(synset, synsets_map, configuration):
        return RelationSet.__get_synsets_from_structure_relations(synset, synsets_map, configuration.hypernyms, configuration.hypernyms_level)

    @staticmethod
    def __get_hyponyms(synset, synsets_map, configuration):
        return RelationSet.__get_synsets_from_structure_relations(synset, synsets_map, configuration.hyponyms, configuration.hyponyms_level)

    @staticmethod
    def __get_others_relations(synset, synsets_map, relations):
        # TODO: sprawdzić, czy ta metoda działa poprawnie
        # TODO: można zrobić coś takeigo, żeby przy każdym synsecie zapisywało, jaka to jest relacja
        related_synsets = []
        for relation_type, relations in synset.relations.items():
            if relation_type in relations:
                related_synsets.extend(relations)
        return related_synsets
