class Unit:

    def __init__(self, id, word, variant, pos, domain, synset):
        self.id = id
        self.word = word
        self.variant = variant
        self.pos = pos
        self.domain = domain
        self.synset = synset


class Synset:

    def __init__(self, id):
        self.id = id
        self.units = []
        self.relations = {}


class Relations:
    SOURCE = 0
    TARGET = 1
    TYPE = 2

    def __init__(self):
        self.__relations = {}

    def add(self, source_id, target_id, relation_id):
        entry = (source_id, target_id, relation_id)
        self.__add_to_map(entry)

    def __add_to_map(self, entry):
        source_id = entry[Relations.SOURCE]
        if source_id not in self.__relations:
            self.__relations[source_id] = [entry]
        else:
            self.__relations[source_id].append(entry)

    def get_all_relations(self):
        relations = []
        for id, entries in self.__relations.items():
            for entry in entries:
                relations.append(entry)
        return relations

    def get_relations_by_id(self, source_id):
        if source_id in self.__relations:
            relations = self.__relations[source_id]
            return relations
        return []

    def get_relations(self, source_id, types_ids):
        if source_id in self.__relations:
            relations = self.__relations[source_id]
            relations = [relation for relation in relations if relation[Relations.TYPE] in types_ids]
            return relations
        return []
