from embeddings.saver import EmbeddingsSetSaver
from wordnet.saver import RelationSetSaver, SynsetSaver


class Diagnostics:

    @staticmethod
    def run(wordnet_set_path, embeddings_set_path, synsets_path, output_path):
        wordnet_set = RelationSetSaver.load(wordnet_set_path)
        embeddings_set = EmbeddingsSetSaver.load(embeddings_set_path)
        synsets = SynsetSaver.load(synsets_path)

        not_intersection_words = []
        for id, relations in wordnet_set.items():
            variant, word = Diagnostics.__get_synset_head(id, synsets)
            if word in embeddings_set:
                entry_map = dict(embeddings_set[word])
                intersection, scores = Diagnostics.__find_intersection(entry_map, relations)
                if len(intersection) == 0:
                    not_intersection_words.append((id, word, variant, len(relations), len(entry_map)))
        Diagnostics.__save_to_file(not_intersection_words, output_path)

    @staticmethod
    def __save_to_file(not_intersection_words, path):
        with open(path, 'w') as file:
            for entry in not_intersection_words:
                file.write('{}:{}:{}:{}:{}\n'.format(entry[0], entry[1], entry[2], entry[3], entry[4]))

    @staticmethod
    def __get_synset_head(id, synsets):
        synset = synsets[id]
        unit = synset.units[0]
        word = unit.word
        variant = unit.variant
        return variant, word

    @staticmethod
    def __find_intersection(entry_map, relations):
        scores = []
        intersection = []
        for relation_word in relations:
            if relation_word in entry_map:
                scores.append(entry_map[relation_word])
                intersection.append(relation_word)
        return intersection, scores











