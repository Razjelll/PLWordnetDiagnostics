from gensim.models import KeyedVectors


class EmbeddingsSet:

    @staticmethod
    def generate(embeddings_path, words, number_similar_words):
        kbests = {}
        model = EmbeddingsSet.__load_model(embeddings_path)
        # TODO: spróbować to zrobić bez przekazywania słów
        for word in words:
            if word in model.wv.vocab:
                kbest = model.similar_by_word(word, number_similar_words)
                kbests[word] = kbest
        return kbests

    @staticmethod
    def __load_model(path):
        model = KeyedVectors.load_word2vec_format(path, binary=False)
        return model
