class EmbeddingsSetSaver:
    DELIMITER = '='
    WORDS_DELIMITER = ','
    BESTS_DELIMITER = ':'

    @staticmethod
    def save(kbests, path):
        with open(path, 'w') as file:
            for word, bests in kbests.items():
                bests_str = EmbeddingsSetSaver.__prepare_bests_str(bests)
                file.write(word)
                file.write(EmbeddingsSetSaver.DELIMITER)
                file.write(bests_str)
                file.write('\n')

    @staticmethod
    def __prepare_bests_str(bests):
        bests_list = ['{0}{2}{1}'.format(tuple[0], str(tuple[1]), EmbeddingsSetSaver.BESTS_DELIMITER) for tuple in bests]
        best_str = EmbeddingsSetSaver.WORDS_DELIMITER.join(bests_list)
        return best_str

    @staticmethod
    def load(path):
        result = {}
        with open(path, 'r') as file:
            for line in file:
                line = line.rstrip()
                parts = line.split(EmbeddingsSetSaver.DELIMITER)
                word = parts[0]
                bests = EmbeddingsSetSaver.__read_bests_str(parts[1])
                result[word] = bests
        return result

    @staticmethod
    def __read_bests_str(bests_str):
        result = []
        bests = bests_str.split(EmbeddingsSetSaver.WORDS_DELIMITER)
        for value in bests:
            parts = value.split(EmbeddingsSetSaver.BESTS_DELIMITER)
            word = parts[0]
            score = parts[1]
            result.append((word, score))
        return result